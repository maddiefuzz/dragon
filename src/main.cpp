// Base Platform Libraries
#include <Arduino.h>
#include <Hash.h>

// Display Libraries
#include <U8g2lib.h>
#include <SPI.h>
#include <Wire.h>

// Wireless Libraries
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>

// Config
#include "config.h"

U8G2_SSD1309_128X64_NONAME2_1_4W_HW_SPI disp(U8G2_R0, /* cs=*/ 15, /* dc=*/ 2, /* reset=*/ 16);

// Socket data variables
double progress = 0.0;
uint64 timeRemaining = 0;
uint64 timeElapsed = 0;
uint8 hotendTemp = 0;
uint8 buildPlateTemp = 0;

// TODO:
// 1. Add WPA2 and connect QR to display
//  "WIFI:S:"+ssid+";T:WPA;P:"+pass+";;"
// 2. Use config.h for printer spec for now
// 3. Add webfig menu for selecting a printer

void statusMsg(String msg){
  disp.firstPage();
  do {
    disp.drawUTF8(110, 1, msg.c_str());
  }while ( disp.nextPage() );
}

void screenRefresh(){
  // Fill screen
  disp.setDrawColor(0);
  disp.drawBox(0, 0, 128, 64);
  delay(20);
  disp.setDrawColor(1);
}

void configModeCallback (WiFiManager *myWiFiManager) {
  Serial.println("Entered config mode");
  statusMsg("Setup");
  Serial.println(WiFi.softAPIP());
  // print the ssid that we should connect to to configure the ESP8266
  Serial.print("Created config portal AP ");
  Serial.println(myWiFiManager->getConfigPortalSSID());
}

void drawUI(){
  disp.drawFrame(2,2,124,20);
  disp.setFont(u8g2_font_6x12_mf);
  disp.drawUTF8(100, 57, "%");
}

void drawTime(int xOffset, String label, uint64 time){
  disp.setFont(u8g2_font_simple1_tf);
  disp.drawUTF8(xOffset, 25, label.c_str());
  disp.setFont(u8g2_font_4x6_tf);
  uint64 hours = time / 3600;
  uint64 minutes = (time % 3600) / 60;
  uint64 seconds = (time % 3600) % 60;
  String timeStr = "";
  if (hours < 10){
    timeStr += "0";
  }
  timeStr += String(hours);
  timeStr += ":";
  if (minutes < 10){
    timeStr += "0";
  }
  timeStr += String(minutes);
  timeStr += ":";
  if (seconds < 10){
    timeStr += "0";
  }
  timeStr += String(seconds);
  disp.drawUTF8(xOffset-10, 25, timeStr.c_str());
}

void drawTemp(int xOffset, String label, int temperature){
  disp.setFont(u8g2_font_tom_thumb_4x6_t_all);
  disp.drawUTF8(xOffset, 25, label.c_str());
  disp.setFont(u8g2_font_luRS08_tf);
  String temperatureString = "";
  if (temperature < 100 && temperature >= 10){
    temperatureString += "0";
  }else if(temperature < 10){
    temperatureString += "00";
  }
  temperatureString += String(temperature);
  temperatureString += " C";
  disp.drawUTF8(xOffset-10, 25, temperatureString.c_str());
}

void drawProgress(){
  // Progress Bar
  int mappedProgress = map(progress, 0, 100, 0, 124);
  disp.drawBox(2, 2, mappedProgress, 20);

  // Percentage Label
  disp.setFont(u8g2_font_profont29_mf);
  String progressStr = (((int)progress < 10)?"0":"")+String((int)progress);
  disp.drawUTF8(100, 25, progressStr.c_str());

  // Hotend Temperature
  int temperature = (int)(progress*10); // TODO: Replace with actual temperature
  drawTemp(90, "Hotend", temperature);

  // Build Plate Temperature
  drawTemp(70, "Bed", temperature);

  // Time Elapsed
  drawTime(40, "Elapsed", progress*10); // TODO: Replace with actual time

  // Time Remaining
  drawTime(20, "Left", progress*10); // TODO: Replace with actual time
}

void setup(void) {
  // Initialize Serial
  Serial.begin(115200);

  // Initialize Display
  disp.begin();
  // disp.setDisplayRotation(U8G2_R1); // Half of the display is broken with this
  disp.setFont(u8g2_font_6x10_tf);
  disp.setFontDirection(1);
  // Serial.println(disp.getDisplayWidth());
  statusMsg("Connecting");


  // Initialilze Wireless
  WiFiManager wifiManager;
  wifiManager.setAPCallback(configModeCallback);
  wifiManager.setTimeout(180);
  if(!wifiManager.autoConnect(AP_NAME)) {
    Serial.println("failed to connect and hit timeout");
    //reset and try again, or maybe put it to deep sleep
    ESP.reset();
    delay(1000);
  }
  //if you get here you have connected to the WiFi
  Serial.println("connected to " + WiFi.SSID() + " : " + WiFi.localIP().toString());
}


void loop(void) {

  disp.firstPage();
  do {
    disp.setDrawColor(1);
    drawUI();
    drawProgress();
    if(millis() % 500 <= 2){
      Serial.println("Refreshing");
      screenRefresh();
    }
  }while ( disp.nextPage() );

  // TODO: Replace this with data fetch
  progress += 0.1;
  if(progress > 100){
    progress = 0;
  }

  delay(150);
}
